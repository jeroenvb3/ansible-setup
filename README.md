This is the setup to easily start using a new ubuntu install.

1. *New computer*: Install the OS.
2. *New computer*: Install `openssh-server`.
3. *Existing*: Edit `.ssh/config`, add key forwarding for the ip of *New computer*.:

```
Host 192.168.122.37
	ForwardAgent yes
```
change with own ip of *new computer*.

4. *Existing computer*: Edit `/etc/ansible/hosts`, add:

```
[ubuntu_desktop]
192.168.122.37 ansible_sudo_pass=[password] new_user=[my_user]
```
change with own ip of *new computer*.

5. *Existing computer*: `ssh-copy-id username@192.168.122.37`.
change with own ip of *new*, and username to what you used when creating the OS.

6. *Existing computer*: `ssh-add ~/.ssh/id_rsa`.
The key `id_rsa` should have access to gitlab.com.
